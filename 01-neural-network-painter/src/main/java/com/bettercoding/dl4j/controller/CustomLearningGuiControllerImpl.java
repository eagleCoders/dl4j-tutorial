package com.bettercoding.dl4j.controller;

import com.bettercoding.dl4j.utils.ImageUtils;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.springframework.stereotype.Component;

@Component
public class CustomLearningGuiControllerImpl implements CustomLearningGuiController {

    private final int imageWidth = 100;
    private final int imageHeight = 100;
    @FXML
    private ImageView inputImageView;
    @FXML
    private ImageView outputImageView;
    @FXML
    private ImageView expectedImageView;

    private MultiLayerNetwork neuralNetwork;

    private DataSet trainDataSet;

    private INDArray testInput;


    @Override
    public void onInitialize() {
        Image outputImage = ImageUtils.emptyImage(Color.WHITE, imageWidth, imageHeight);
        Image inputImage = new Image(getClass().getResourceAsStream("/data/input.png"));
        Image expectedImage = new Image(getClass().getResourceAsStream("/data/expected.png"));
        inputImageView.setImage(inputImage);
        outputImageView.setImage(outputImage);
        expectedImageView.setImage(expectedImage);

        this.neuralNetwork = createNeuralNetwork();
        trainDataSet = ImageUtils.convertToDataSet(inputImage, expectedImage);
        testInput = trainDataSet.getFeatures(); //Data set contains all pixels
    }

    @Override
    public void onRefreshGUI() {
        INDArray nnOutput = neuralNetwork.output(testInput);
        WritableImage image = ImageUtils.drawImage(nnOutput, imageWidth, imageHeight);
        outputImageView.setImage(image);
    }

    @Override
    public void onTrainLoop(long loopNo) {
        int batchSize = 1000;
        DataSet trainingDataSet = trainDataSet.sample(batchSize);
        neuralNetwork.fit(trainingDataSet);
    }

    @Override
    public void onTestAction() {
        Image inputImage = new Image(getClass().getResourceAsStream("/data/test.png"));
        Image expectedImage = new Image(getClass().getResourceAsStream("/data/expected.png"));
        DataSet testDataSet = ImageUtils.convertToDataSet(inputImage, expectedImage);
        INDArray nnOutput = neuralNetwork.output(testDataSet.getFeatures());
        WritableImage image = ImageUtils.drawImage(nnOutput, imageWidth, imageHeight);
        outputImageView.setImage(image);
    }

    @Override
    public void onTrainAction() {

    }

    @Override
    public MultiLayerNetwork onGetNeuralNetwork() {
        return this.neuralNetwork;
    }

    @Override
    public void onSetNeuralNetwork(MultiLayerNetwork restoreMultiLayerNetwork) {
        this.neuralNetwork = neuralNetwork;
    }

    private MultiLayerNetwork createNeuralNetwork() {
        int seed = ImageUtils.RANDOM.nextInt();
        double learningRate = 0.02;
        int numInputs = 5;   // x, y, R, G, B
        int numHiddenNodes = 200;
        int numOutputs = 3; //R, G , B
        double momentum = 0.8;

        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(seed)
                .weightInit(WeightInit.XAVIER)
                .updater(new Nesterovs(learningRate, momentum))
                .list()
                .layer(0, new DenseLayer.Builder().nIn(numInputs).nOut(numHiddenNodes)
                        .activation(Activation.LEAKYRELU)
                        .build())
                .layer(1, new DenseLayer.Builder().nIn(numHiddenNodes).nOut(numHiddenNodes)
                        .activation(Activation.LEAKYRELU)
                        .build())
                .layer(2, new DenseLayer.Builder().nIn(numHiddenNodes).nOut(numHiddenNodes)
                        .activation(Activation.LEAKYRELU)
                        .build())
                .layer(3, new DenseLayer.Builder().nIn(numHiddenNodes).nOut(numHiddenNodes)
                        .activation(Activation.LEAKYRELU)
                        .build())
                .layer(4, new OutputLayer.Builder(LossFunctions.LossFunction.L2)
                        .activation(Activation.IDENTITY)
                        .nIn(numHiddenNodes).nOut(numOutputs).build())
                .pretrain(false).backprop(true).build();

        MultiLayerNetwork net = new MultiLayerNetwork(conf);
        net.init();

        return net;
    }
}
